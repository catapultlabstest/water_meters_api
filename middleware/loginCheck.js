module.exports = (app) => {
  app.set('loginCheck', (req, res, next) => {
    if (!req.loginSession.user) {
      return res.status(401).send({ message: 'Please log in!' })
    }
    return next()
  })
}