var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var session = require('client-sessions')
var mongo = require('mongodb')

// enable CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', 'http://localhost:8080')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.use(bodyParser.json())

app.use(session({
  cookieName: 'loginSession',
  secret: 'amazingStrIngW1ThNum3eR5',
  duration: 30 * 60 * 1000,
  activeDuration: 30 * 60 * 1000
}))

//connect to db
var url = 'mongodb://localhost:27017/water-company'

mongo.connect(url)
.then((db) => {
  //register middleware
  require('./middleware/loginCheck')(app)

  //register routes
  require('./routes/user')(app, db)
  require('./routes/dashboard')(app, db)
})
.catch((error) => {
  console.log(error)
})

app.listen(3000, () => {
  console.log('App listening on port 3000!')
})