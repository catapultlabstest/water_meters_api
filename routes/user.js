module.exports = (app, database) => {
  let bcrypt = require('bcrypt')

  let login = (req, res) => {
    database.collection('users')
      .findOne({
        email: req.body.email
      })
      .then((user) => {
        if(!user) {
          return res.status(401).send({ message: 'Username and password do not match' })
        }

        return bcrypt.compare(req.body.password, user.password).then((hash) => {
          if (hash) {
            delete user.password
            req.loginSession.user = user
            return res.send(user)
          }

          return res.status(401).send({ message: 'Username and password do not match' })
        })
      })
      .catch((error) => {
        return res.status(500).send({ message: 'Internal server error for: Login' })
      })
  }

  let signup = (req, res) => {
    let user = req.body

    if (!user.email || !user.password) {
      return res.status(409).send({ message: 'Please fill out both fields' })
    }

    database.collection('users')
      .findOne({
        email: req.body.email
      })
      .then((record) => {
        if (record) {
          return res.status(409).send({ message: 'E-mail already exists!' })
        }

        return bcrypt.hash(req.body.password, 10).then((hash) => {
          user.password = hash

          return database.collection('users')
            .insertOne(user)
            .then((response) => {
              let insertedUser = {
                _id: response.insertedId,
                email: req.body.email
              }

              req.loginSession.user = insertedUser
              return res.send(insertedUser)
            })
        })
      })
      .catch((error) => {
        return res.status(500).send({ message: 'Internal server error for: Signup' })
      })
  }

  let logout = (req, res) => {
    req.loginSession.reset()
    return res.send()
  }

  let checkSession = (req, res) => {
    if (req.loginSession && req.loginSession.user) {

      database.collection('users')
        .findOne({
          email: req.loginSession.user.email
        })
        .then((user) => {
          delete user.password
          return res.send(user)
        })
        .catch((error) => {
          return res.status(500).send({ message: 'Internal server error for: Authenticating' })
        })
    } else {
      return res.status(401).send({ message: 'Please log in' })
    }
  }

  app.post('/api/login', login)
  app.post('/api/signup', signup)
  app.get('/api/logout', logout)
  app.get('/api/checkSession', checkSession)
}