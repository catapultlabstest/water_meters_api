module.exports = (app, database) => {
  let loginCheck = app.get('loginCheck')

  let combined = (req, res) => {
    database.collection('readings').aggregate([
      {
        $group: {
          _id: { $dateToString: { format: '%Y-%m-%d', date: '$date' }},
          reading: { $sum: '$reading' }
        }
      },
      {
        $project: {
          'date': '$_id',
          reading: 1,
          _id: 0
        }
      },
      {
        $sort: {
          date: 1
        }
      }
    ], (error, data) => {
      if (error) return res.status(500).send({ message: 'Internal server error for: Combined readings' })
      return res.send(data)
   })
  }

  let groupedReadingsByMeters = (req, res) => {
    let toTime = new Date(req.body.to)
    let fromTime = new Date(req.body.from)

    database.collection('readings').aggregate([
      {
        $match: {
          date: {
            $gte: fromTime,
            $lt: toTime
          }
        } 
      },
      {
        $lookup: {
          from: 'meters',
          localField: 'meterId',
          foreignField: '_id',
          as: 'contact'
        }
      },
      {
        $group: {
           _id: { meter: '$meterId', contact: '$contact' },
           reading: { $sum: '$reading' }
        }
      },
      {
        $project: {
          'meterId': '$_id.meter',
          'contact': { 
            $arrayElemAt: [ '$_id.contact', 0 ]
          },
          reading: 1,
          _id: 0
        }
      },
      {
        $sort: {
          meterId: 1
        }
      }
    ], (error, data) => {
      if (error) return res.status(500).send({ message: 'Internal server error for: Grouped readings by meters' })
      return res.send(data)
   })
  }

  app.get('/api/dashboard/combinedReadings', loginCheck, combined)
  app.post('/api/dashboard/groupedReadingsByMeters', loginCheck, groupedReadingsByMeters)
}